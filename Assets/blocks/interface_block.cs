using UnityEngine;

public class interface_block : MonoBehaviour
{
    public GameObject InterfaceViz;
    public GameObject HighlightViz;
    
    public void HighlightOn ()
    {
        HighlightViz.SetActive(true);
    }
    
    
    public void HighlightOff ()
    {
        HighlightViz.SetActive(false);
    }
}