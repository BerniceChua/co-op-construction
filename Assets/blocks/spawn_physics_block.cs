using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawn_physics_block : MonoBehaviour 
{
    
    [System.NonSerialized]
        public Transform DynamicParent;
    
    public Vector3 ForceVector;
    public float ForceAmount;
    
    public void Construct (Transform DynamicParent)
    {
        this.DynamicParent = DynamicParent;
    }
    
    public GameObject SpawnBlock (GameObject Prefab_Block)
    {
        GameObject NewBlock = GameObject.Instantiate(Prefab_Block);
        NewBlock.transform.SetParent(DynamicParent);
        NewBlock.transform.position = this.transform.position;
        
        Rigidbody2D BlockRB = NewBlock.GetComponent<Rigidbody2D>();
        BlockRB.AddForce(ForceVector.normalized * ForceAmount);
        
        return NewBlock;
    }
}
