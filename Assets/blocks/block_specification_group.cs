using UnityEngine;

public enum block_type
{
    BlockType_Invalid,
    
    BlockType_SquareCrescent,
    BlockType_HalfCircle,
    BlockType_Tall,
    BlockType_L,
    BlockType_Crooked,
    BlockType_EquilateralTriangle,
    BlockType_Crescent,
    BlockType_Parallelogram,
    BlockType_Hourglass,
    BlockType_Diamond,
    BlockType_WobblyX,
    BlockType_DoubleSquiggle,
    BlockType_SingleSquiggle,
    BlockType_TallTriangle,
    BlockType_SpaceInvader,
    BlockType_Pyramid,
    BlockType_Cross,
    
    BlockType_Count,
};

[System.Serializable]
public class block_specification
{
    public GameObject Prefab_PhysicsBlock;
    public GameObject Prefab_InterfaceBlock;
    public block_type BlockType;
}

[CreateAssetMenu (menuName="Block Specification Group")]
public class block_specification_group : ScriptableObject
{
    public block_specification[] Blocks;
    
    public int IndexOfSpecification (block_type Type)
    {
        int Result = -1;
        for (int i = 0; i < Blocks.Length; i++)
        {
            if (Blocks[i].BlockType == Type)
            {
                Result = i;
                break;
            }
        }
        return Result;
    }
}