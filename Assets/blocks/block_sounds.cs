using UnityEngine;

public class block_sounds : MonoBehaviour
{
    private int LastPlayedSound = 0; 
    public AudioClip[] CollisionSounds;
    public AudioSource CollisionPlayer;
    
    public void OnCollisionEnter2D ()
    {
        int NextSound = LastPlayedSound + 1;
        if (NextSound >= CollisionSounds.Length)
        {
            NextSound= 0;
        }
        CollisionPlayer.PlayOneShot(CollisionSounds[NextSound]);
        
        LastPlayedSound = NextSound;
    }
}