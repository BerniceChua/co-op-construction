using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using InControl;
using TMPro;

public class GameLoopManager : MonoBehaviour {
    
    private InputDevice m_p1Device;
    private InputDevice m_p2Device;
    
    [SerializeField] public float m_StartDelay = 3f;             // The delay between the start of RoundStarting and RoundPlaying phases.
    [SerializeField] public float m_EndDelay = 3f;
    [SerializeField] public float m_EndDelayCataclysm = 3.0f;
    public TextMeshProUGUI m_MessageText;                  // Reference to the overlay Text to display result text, etc.
    //public GameObject m_PlayerPrefab;             // Reference to the prefab the players will control.
    [SerializeField] public PlayersManager[] m_PlayersArray;
    
    private WaitForSeconds m_StartWait;         /// Used to have a delay whilst the round starts.
    private WaitForSeconds m_EndWait;           /// Used to have a delay whilst the round or game ends.
    private WaitForSeconds m_EndWaitCataclysm;  /// Used to delay message and UI after cataclysm appears.
    private PlayersManager m_RoundCasualty;          /// Reference to the casualty of the current round.  Possibly will be used to make an announcement of who died?
    
    [SerializeField] public GameObject m_CountdownTimerPanel;
    /// Timer-related UI
    [SerializeField] public float m_TimerMaximumInMinutes;
    private float m_TimerMaximumInSeconds;
    [SerializeField] private TextMeshProUGUI m_countdownTimerText;
    private float m_currentTime;
    string minutes;
    string seconds;
    string fraction;
    
    /// <summary>
    /// MainMenu options-related UI (restart, main menu, quit)
    /// </summary>
    [Tooltip("Put MainMenuPanel here.")]
        [SerializeField] private GameObject m_menuPanel;
    
    /// <summary>
    /// Cataclysmic event's variables
    /// </summary>
    [SerializeField] public GameObject m_CataclysmicEventPrefab;
    public Transform[] MeteorSpawner;
    [SerializeField] private Transform m_CataclysmicEventContainer;
    
    [Tooltip("For scene loading, put the name of the scene for the intro")]
        [SerializeField] private string m_introScene;
    [Tooltip("For scene loading, put the name of the scene for the game level")]
        [SerializeField] private string m_gameLevelScene;
    
    public AudioSource EndSoundSource;
    public AudioSource Soundtrack;
    public bool PlayingEndSound;
    
    // Use this for initialization
    void Start() {
        /// <summary>
        /// Initialize controller inputs.
        /// </summary>
        if (InputManager.Devices.Count > 0)
            m_p1Device = InputManager.Devices[0];
        if (InputManager.Devices.Count > 1)
            m_p2Device = InputManager.Devices[1];
        
        /// convert seconds to milliseconds
        m_TimerMaximumInSeconds = m_TimerMaximumInMinutes * 60.0f;
        
        m_StartWait = new WaitForSeconds(m_StartDelay);
        m_EndWait = new WaitForSeconds(m_EndDelay);
        m_EndWaitCataclysm = new WaitForSeconds(m_EndDelayCataclysm);
        
        ResetTimer();
        StartCoroutine(GameLoop());
    }
    
    // Update is called once per frame
    void Update() {
        
    }
    
    private IEnumerator GameLoop() {
        //ResetTimer();
        // Start off by running the 'RoundStarting' coroutine but don't return until it's finished.
        yield return StartCoroutine(RoundStarting());
        
        // Once the 'RoundStarting' coroutine is finished, run the 'RoundPlaying' coroutine but don't return until it's finished.
        yield return StartCoroutine(RoundPlaying());
        
        // Once execution has returned here, run the 'RoundEnding' coroutine, again don't return until it's finished.
        yield return StartCoroutine(RoundEnding());
        
        /// Re-enable restart/main menu/quit options when game ends.
        m_menuPanel.SetActive(true);
        PlayingEndSound = false;
        EndSoundSource.Stop();
        Soundtrack.Play();
        /// After 'RoundEnding()' has finished, check if player wants to play again, or go to main menu, or quit.
        /// These are for either controller buttons or keyboard shortcuts, if the players don't use the UI buttons.
        if (Input.GetKey(KeyCode.Return) || (m_p1Device != null && m_p1Device.MenuWasPressed)) {
            // Restart the level.
            SceneManager.LoadScene(m_gameLevelScene);
        } else if (Input.GetKey(KeyCode.Escape) || (m_p1Device != null && m_p1Device.MenuWasPressed)) {
            // Go to main menu.
            SceneManager.LoadScene(m_introScene);
        } else if (Input.GetKey(KeyCode.Q) || (m_p1Device != null && m_p1Device.MenuWasPressed)) {
            // Quit the game.
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }
        
    }
    
    private IEnumerator RoundStarting() {
        // As soon as the round starts reset the players and make sure they can't move.
        ResetTimer();
        ResetAllPlayers();
        //DisablePlayerControl();
        
        // Wait for the specified length of time until yielding control back to the game loop.
        yield return m_StartWait;
    }
    
    private IEnumerator RoundPlaying() {
        
        /// Disable restart/main menu/quit options when game starts.
        m_menuPanel.SetActive(false);
        
        // As soon as the round begins playing let the players control the characters.
        EnablePlayerControl();
        
        // Clear the text from the screen.
        m_MessageText.text = string.Empty;
        
        // As soon as the round begins playing, start the countdown timer.
        while (m_currentTime > 0.0f) {
            UpdateTimer();
            // ... return on the next frame.
            yield return null;
        }
        m_CountdownTimerPanel.SetActive(false);
        
        yield return StartCoroutine(CataclysmicEvent());
        
        //yield return m_EndWaitCataclysm;
    }
    
    private IEnumerator RoundEnding() {
        // Stop players from moving.
        DisablePlayerControl();
        
        // Clear the result from the previous round.
        m_RoundCasualty = null;
        
        // See if there is a casualty now the round is over.
        m_RoundCasualty = FindCasualty();
        
        // If there is a casualty, increment their score.
        if (m_RoundCasualty != null) {
            m_RoundCasualty.m_IsCasualty = true;
        }
        
        yield return m_EndDelayCataclysm;
        
        /// Get a message based on the scores and whether or not all the characters survived and display it.
        string message = EndMessage();
        m_MessageText.text = message;
        
        // Wait for the specified length of time until yielding control back to the game loop.
        yield return m_EndWait;
    }
    
    // This is used to check if there is one or fewer players remaining and thus the round should end.
    private bool OneTankLeft() {
        // Start the count of players left at zero.
        int numTanksLeft = 0;
        
        // Go through all the players...
        for (int i = 0; i < m_PlayersArray.Length; i++) {
            // ... and if they are active, increment the counter.
            if (m_PlayersArray[i].m_Instance.activeSelf)
                numTanksLeft++;
        }
        
        // If there are one or fewer players remaining return true, otherwise return false.
        return numTanksLeft <= 1;
    }
    
    /// This function is to find out if there is a character who died by the end of the round.
    /// This function is called with the assumption that 1 or fewer players are currently active.
    private PlayersManager FindCasualty() {
        /// Go through all the players...
        for (int i = 0; i < m_PlayersArray.Length; i++) {
            /// ... and if at least one of them is dead, the game is over.
            if (m_PlayersArray[i].m_IsCasualty)
                //roundIsLost = true;
                return m_PlayersArray[i];
        }
        
        /// If none of the characters died, set lose to false and return null.
        //roundIsLost = false;
        return null;
    }
    
    private string EndMessage() {
        // By default when a round ends, and all the characters survived, show the victory message.
        string message = "";
        
        // If there is a casualty then change the message to reflect that.
        if (m_RoundCasualty == null)
            message = "EVERYONE SURVIVED!";
        
        // If there is a casualty then change the message to reflect that.
        if (m_RoundCasualty != null)
            message = "YOU DIED";
        
        // Add some line breaks after the initial message.
        message += "\n\n\n\n";
        
        // Go through all the players and check if any player became a casualty.
        for (int i = 0; i < m_PlayersArray.Length; i++) {
            message += m_PlayersArray[i].name + " was killed.\n";
        }
        
        return message;
    }
    
    // This function is used to reset the timer.
    private void ResetTimer() {
        m_currentTime = m_TimerMaximumInSeconds;
        minutes = Mathf.Floor(m_currentTime / 60).ToString("00");
        seconds = (m_currentTime % 60).ToString("00");
        fraction = ((m_currentTime * 100) % 100).ToString("000");
        m_countdownTimerText.text = "Time Left: " + minutes + ":" + seconds + ":" + fraction;
    }
    
    // This function is used to turn all the player-characters back on and reset their positions and properties.
    private void ResetAllPlayers() {
        for (int i = 0; i < m_PlayersArray.Length; i++) {
            m_PlayersArray[i].Reset();
        }
    }
    
    private void EnablePlayerControl() {
        for (int i = 0; i < m_PlayersArray.Length; i++) {
            m_PlayersArray[i].EnableControl();
        }
    }
    
    private void UpdateTimer() {
        if (m_countdownTimerText != null) {
            m_currentTime -= Time.deltaTime;
            minutes = Mathf.Floor(m_currentTime / 60).ToString("00");
            seconds = (m_currentTime % 60).ToString("00");
            fraction = ((m_currentTime * 100) % 100).ToString("000");
            m_countdownTimerText.text = "Time Left: " + minutes + ":" + seconds + ":" + fraction;
        }
        
        if (m_currentTime < 7 && !PlayingEndSound)
        {
            Soundtrack.Stop();
            EndSoundSource.Play();
            PlayingEndSound = true;
        }
    }
    
    private IEnumerator CataclysmicEvent() {
        Debug.Log("Play cataclysmic event now.");
        GameObject meteor;
        
        int numberOfFalls = Random.Range(10, 15);
        /*
        float[] sourcesOfCataclysmArray = new float[] { -12, 0, 12 };
        float sourceOfCataclysm = Random.Range(-12.0f, 12.0f);
        */
        
        //float thrust = Mathf.Sin( Random.Range(-10000, 10000) );
        float[] rangeOfThrusts = new float[] { -500, -400, 0, 400, 500 };
        float thrust = 5000;// rangeOfThrusts[Random.Range(0, rangeOfThrusts.Length - 1)];
        
        /*
        if (sourceOfCataclysm <= -3) {
            Debug.Log("inside 'if (sourceOfCataclysm <= -3)'");
            thrust = rangeOfThrusts[Random.Range(3, rangeOfThrusts.Length - 1)];
        } else if (sourceOfCataclysm > -3 && sourceOfCataclysm < 3) {
            Debug.Log("inside 'else if (sourceOfCataclysm > -3 && sourceOfCataclysm < 3)'");
            thrust = rangeOfThrusts[2];
        } else if (sourceOfCataclysm >= 3) {
            Debug.Log("inside 'else if (sourceOfCataclysm >= 3)'");
            thrust = rangeOfThrusts[Random.Range(0, 1)];
        }
        */
        
        for (int i = 0; i < numberOfFalls; i++) {
            int RandomSpawner = Random.Range(0, MeteorSpawner.Length - 1);
            meteor = Instantiate (m_CataclysmicEventPrefab);
            meteor.transform.position = MeteorSpawner[RandomSpawner].position;
            meteor.transform.rotation = MeteorSpawner[RandomSpawner].rotation;
            Rigidbody2D RB = meteor.GetComponent<Rigidbody2D>();
            RB.AddForce (MeteorSpawner[RandomSpawner].transform.up * thrust);
            
            yield return new WaitForSecondsRealtime(Random.Range(1, 3));
            
            if (meteor && meteor.transform.position.y < -10.0f) {
                Destroy(meteor);
            }
            
            //yield return m_EndDelayCataclysm;
        }
    }
    
    private void DisablePlayerControl() {
        for (int i = 0; i < m_PlayersArray.Length; i++) {
            m_PlayersArray[i].DisableControl();
			m_PlayersArray[i].GetComponent<character_movement>().playerMode = PlayerMode.Sheltering;
            
		}
    }
    
    /// <summary>
    /// UI Button to restart the level.
    /// </summary>
    public void UIButtonRestartGame() {
        SceneManager.LoadScene(m_gameLevelScene);
    }
    
    /// <summary>
    /// UI Button to go to the main menu.
    /// </summary>
    public void UIButtonGoToMainMenu() {
        SceneManager.LoadScene(m_introScene);
    }
    
    /// <summary>
    /// UI Button to quit the game.
    /// </summary>
    public void UIButtonQuitGame() {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
