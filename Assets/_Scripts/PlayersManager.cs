﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayersManager : MonoBehaviour {

    // This class works with the GameManager class to control how the player controls behave
    // and whether or not players have control in the different phases of the game.

    public Transform m_SpawnPoint;                          // The position and direction the player will have when it spawns.
    [HideInInspector] public GameObject m_Instance;         // A reference to the instance of the player when it is created.
    [HideInInspector] public bool m_IsCasualty;

    //private PlayerMovement m_PlayerMovement;                  // Reference to player's movement script, used to disable and enable control.
    private GameObject m_CanvasGameObject;                  // Used to disable the world space UI during the Starting and Ending phases of each round.

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    public void Setup() {
        // Get references to the components.
        //m_PlayerMovement = m_Instance.GetComponent<PlayerMovement>();
        //m_CanvasGameObject = m_Instance.GetComponentInChildren<Canvas>().gameObject;

        // Set the player numbers to be consistent across the scripts.
        //m_PlayerMovement.m_PlayerNumber = m_PlayerNumber;

        // Get all of the renderers of the player characters.
        MeshRenderer[] renderers = m_Instance.GetComponentsInChildren<MeshRenderer>();

    }


    // Used during the phases of the game where the player shouldn't be able to control their characters.
    public void DisableControl() {
        //m_PlayerMovement.enabled = false;
        //m_CanvasGameObject.SetActive(false);
    }


    // Used during the phases of the game where the player should be able to control their characters.
    public void EnableControl() {
        //m_PlayerMovement.enabled = true;
        //m_CanvasGameObject.SetActive(true);
    }

    // Used at the start of each round to put the players into their default state.
    public void Reset() {
		m_Instance = gameObject;
		m_SpawnPoint = transform;
        m_Instance.transform.position = m_SpawnPoint.position;
        m_Instance.transform.rotation = m_SpawnPoint.rotation;

        m_Instance.SetActive(false);
        m_Instance.SetActive(true);
    }

}
