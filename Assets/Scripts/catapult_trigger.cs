using UnityEngine;

public class catapult_trigger : MonoBehaviour
{
    public Collider2D Trigger;
    
    public Rigidbody2D[] OverlappingColliders;
    public int OverlappingCollidersCount;
    
    public void Construct ()
    {
        int MaxOverlappingColliders = 10;
        OverlappingColliders = new Rigidbody2D[MaxOverlappingColliders];
        OverlappingCollidersCount = 0;
    }
    
    public void OnTriggerEnter2D (Collider2D Other)
    {
        if (OverlappingCollidersCount < OverlappingColliders.Length)
        {
            Rigidbody2D RB = Other.GetComponent<Rigidbody2D>();
            if (RB)
            {
                OverlappingColliders[OverlappingCollidersCount++] = RB;
            }
        }
    }
    
    public void OnTriggerExit2D (Collider2D Other)
    {
        int IndexOfCollider = -1;
        for (int i = 0; i < OverlappingCollidersCount; i++)
        {
            if (Other.gameObject == OverlappingColliders[i].gameObject)
            {
                IndexOfCollider = i;
                break;
            }
        }
        
        for (int j = IndexOfCollider; j < OverlappingCollidersCount - 1; j++)
        {
            OverlappingColliders[j] = OverlappingColliders[j + 1];
        }
        OverlappingColliders[OverlappingCollidersCount - 1] = null;
        OverlappingCollidersCount--;
    }
}