using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

public class character_movement : MonoBehaviour {
    
    public float HorizontalSpeed;
    
    public bool RampUpToFullSpeed;
    public float RampUpTime;
    [SerializeField]
        public float TimeSinceMovementStart;
    
    public Animator CharacterAnimator;
    
    [System.NonSerialized]
        public string Animator_RunFloatName = "run";
    
    
	public List<GameObject> inRange;
    
	public PlayerMode playerMode;
    
	public KeyCode[] leftrightkeys;
    public KeyCode KeyCode_NextInventoryItem;
    public KeyCode KeyCode_SelectInventoryItem;
    public KeyCode KeyCode_PickUpAndDropBuildObject;
	public KeyCode KeyCode_RotateObject;
    
    [System.NonSerialized]
        public block_inventory BlockInventory;
    
    [System.NonSerialized]
        public BuildObject SelectedBuildObject;
    
    [System.NonSerialized]
        public ObjectMover MainObjectMover;
    
	public InputDevice inputDevice;
    
	public void Construct (block_inventory BlockInventory, ObjectMover MainObjectMover, int deviceIndex)
    {
        this.BlockInventory = BlockInventory;
        this.MainObjectMover = MainObjectMover;
        SelectedBuildObject = null;
		//if there is a device at deviceIndex, assign it to our device variable
		foreach(InputDevice id in InputManager.Devices)
			Debug.Log("device: "+ id);
		
        if (InputManager.Devices.Count > deviceIndex) {
			Debug.Log ("assign device at index: " + deviceIndex);
			inputDevice = InputManager.Devices [deviceIndex];
		}
		GameObject.FindObjectOfType<GameLoopManager>().m_PlayersArray[deviceIndex] = GetComponent<PlayersManager>();
        
		GetComponent<PlayerHitDetection> ().Construct (this);
    }
    
    void Update ()
	{
        if (playerMode == PlayerMode.Running) 
        {
			
			// Inventory
			if (Input.GetKeyDown (KeyCode_NextInventoryItem) ||
				(inputDevice != null && inputDevice.DPadUp.WasPressed))
            {
                BlockInventory.SelectNextBlock();
            }
			if (Input.GetKeyDown (KeyCode_SelectInventoryItem) ||
				(inputDevice != null && inputDevice.DPadDown.WasPressed))
            {
                BlockInventory.SpawnSelectedBlock();
            }
            
            
            // Movement
            if (Input.GetKey (leftrightkeys[0]) ||
				(inputDevice != null && inputDevice.DPadLeft.IsPressed))
            {
                // Reset Timer
				if (Input.GetKeyDown (leftrightkeys[0]) ||
					(inputDevice != null && inputDevice.DPadLeft.WasPressed && inputDevice.DPadLeft.LastState == false))
                {
					TimeSinceMovementStart = 0;
				}
	            
				Move (-1);
				TimeSinceMovementStart += Time.deltaTime;
			} 
			else if (Input.GetKey (leftrightkeys[1]) ||
                     (inputDevice != null && inputDevice.DPadRight.IsPressed)) 
            {
				// Reset Timer
				if (Input.GetKeyDown (leftrightkeys[1]) ||
					(inputDevice != null && inputDevice.DPadLeft.WasPressed && inputDevice.DPadRight.LastState == false))
                {
					TimeSinceMovementStart = 0;
				}
	            
				Move (1);
				TimeSinceMovementStart += Time.deltaTime;
			} 
            else 
            {
				float MoveSpeed = 0.0f;
				
                if (RampUpToFullSpeed && TimeSinceMovementStart > 0) 
                {
					TimeSinceMovementStart -= Time.deltaTime;
					float HalfRampTime = RampUpTime / 2;
					float PercentRampUp = TimeSinceMovementStart - HalfRampTime / HalfRampTime;
					PercentRampUp = Mathf.Clamp (PercentRampUp, 0.0f, 1.0f);
					MoveSpeed = PercentRampUp;
				}
                
				CharacterAnimator.SetFloat (Animator_RunFloatName, MoveSpeed);
			}
            
            // Pick Up
			if (Input.GetKey (KeyCode_PickUpAndDropBuildObject) ||
				(inputDevice != null && inputDevice.Action1.WasPressed))
            {
                if (SelectedBuildObject)
                {
                    float DistanceToSelected = (SelectedBuildObject.transform.position - transform.position).sqrMagnitude;
                    if (DistanceToSelected < SelectedBuildObject.MaxPickupDistance)
                    {
                        MainObjectMover.PickUp (SelectedBuildObject.gameObject, this);
                        playerMode = PlayerMode.Holding;
                    }
                }
            }
		} 
        else if (playerMode == PlayerMode.Holding) 
        {
            // NOTE(Peter): Moving and rotating the object is handled by the object mover since it exists
            
            // Drop Item
			if (Input.GetKeyDown (KeyCode_PickUpAndDropBuildObject) ||
				(inputDevice != null && inputDevice.Action1.WasPressed)) 
            {
                Debug.Log("Put Down");
				MainObjectMover.PutDown(this);
                playerMode = PlayerMode.Running;
			}
		}
    }
    
    public void Move(float XDirection)
    {
        this.transform.localScale = new Vector3(XDirection, 1, 1);
        
        float PercentRampUp = 1.0f;
        if (RampUpToFullSpeed)
        {
            PercentRampUp = TimeSinceMovementStart / RampUpTime;
            PercentRampUp = Mathf.Clamp(PercentRampUp, 0.0f, 1.0f);
            
        }
        
        CharacterAnimator.SetFloat(Animator_RunFloatName, PercentRampUp);
        Vector3 XOffset = Vector3.right * XDirection * HorizontalSpeed * PercentRampUp * Time.deltaTime; 
        this.transform.position = this.transform.position + XOffset;
        
    }
    
    public void SetSelectedBuildObject (BuildObject BO)
    {
        SelectedBuildObject = BO;
    }
    
}

public enum PlayerMode{
	Running,
	Holding,
	Launching,
	Sheltering
}
