using UnityEngine;

public enum spawn_target
{
    SpawnTarget_Invalid,
    
    SpawnTarget_Alien,
    SpawnTarget_Robot,
    SpawnTarget_CatapultLeft,
    SpawnTarget_CatapultRight,
    SpawnTarget_Inventory,
    SpawnTarget_Fence,
    Spawntarget_Count
};

public class spawner : MonoBehaviour
{
    public spawn_target SpawnTarget;
    
    public GameObject Spawn (GameObject Prefab_Target)
    {
        GameObject InstanceTarget = GameObject.Instantiate(Prefab_Target);
        InstanceTarget.transform.position = this.transform.position;
        return InstanceTarget;
    }
}
