﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvItem : MonoBehaviour {

	public GameObject item;
	//public float gameplayPlane;

	public void Start() {
		var sr = gameObject.GetComponent<SpriteRenderer>();
		var isr = item.GetComponent<SpriteRenderer>();
		sr.sprite = isr.sprite;
	}

	public GameObject Spawn(Vector2 coords) {
		return Instantiate(item, coords, Quaternion.identity);
	}

}
