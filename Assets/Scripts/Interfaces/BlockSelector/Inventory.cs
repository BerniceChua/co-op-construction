﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour {

	[SerializeField]
	private Vector2 spawnLoc;
	public Vector2 catapultLoc;
	[SerializeField]
	private GameObject pointer;
	[SerializeField]
	private float additionSpeed;
	[SerializeField]
	private int invSize;//This is the number of blocks availible to choose
	public GameObject[] items;

	private List<GameObject> invItems;
	[System.NonSerialized]
	public List<GameObject> activeItems;
	private float cTime;

	private int cursor;

	private void Awake() {
		cursor = 0;
		invItems = new List<GameObject>();
		cTime = 0;
	}

	private void Update() {
		
		if(invItems.Count < invSize) {
			cTime += Time.deltaTime;
			if(cTime >= additionSpeed) {
				invItems.Add(getRandomItem());
				cTime = 0;
			}
		}

		if(invItems.Count > 0) {
			if(pointer.GetComponent<Renderer>().bounds.center != invItems[cursor].GetComponent<Renderer>().bounds.center) {//Keep pointer centered on item
				pointer.transform.position = invItems[cursor].GetComponent<Renderer>().bounds.center - (pointer.GetComponent<Renderer>().bounds.size / 2);
			}
		} else {
			if(pointer.activeSelf) pointer.SetActive(false);
		}
	}

	public GameObject deployItem() {
		GameObject item = invItems[cursor];
		invItems.RemoveAt(cursor);
		item.transform.position = new Vector3(catapultLoc.x, catapultLoc.y, item.transform.position.z);
		activeItems.Add(item);
		return item;
	}

	private GameObject getRandomItem() {
		if(!pointer.activeSelf) pointer.SetActive(true);
		return Instantiate(items[(int)Random.Range(0, (float)items.Length)], spawnLoc, Quaternion.identity);//TODO: Add random rotation?
	}

	public void moveSelection(int dist) {
		int nc = cursor + dist;

		if(nc < 0) cursor = 0;
		else if(nc >= invSize) cursor = invSize - 1;
		else cursor = nc;
	}
}
