using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class catapult : MonoBehaviour
{
    
    public float ShouldLaunch_MaxDistance;
    
    public Animator CatapultAnimator;
    [System.NonSerialized]
        public string Launch_AnimatorTriggerName = "launch";
    
    public catapult_trigger Trigger;
    // Direction is calculated as (LaunchForce_Direction.position - transform.position).normalized
    public Transform LaunchForce_Direction;
    public float LaunchForce_Thrust;
    
    public Coroutine CheckShouldLaunchLoop;
    
    public AudioSource Creak;
    
    [System.NonSerialized]
        public Transform Player1;
    [System.NonSerialized]
        public Transform Player2;
    
    [System.NonSerialized]
        public Transform LastPlayerToLaunch;
    
    public void Construct (Transform Player1, Transform Player2)
    {
        this.Player1 = Player1;
        this.Player2 = Player2;
        StartCheckShouldLaunchLoop();
        Trigger.Construct();
    }
    
    public void StartCheckShouldLaunchLoop ()
    {
        if (CheckShouldLaunchLoop == null)
        {
            CheckShouldLaunchLoop = StartCoroutine(CoCheckShouldLanch());
        }
    }
    
    public void StopCheckShouldLaunchLoop ()
    {
        if (CheckShouldLaunchLoop != null)
        {
            StopCoroutine(CheckShouldLaunchLoop);
        }
    }
    
    public IEnumerator CoCheckShouldLanch()
    {
        while (true)
        {
            
            // See if we should launch
            Transform WouldLaunch = null;
            float MinDistance = 10000;
            
            float Player1Distance = (Player1.position - transform.position).sqrMagnitude;
            float Player2Distance = (Player2.position - transform.position).sqrMagnitude;
            
            if (Player1Distance < Player2Distance)
            {
                MinDistance = Player1Distance;
                WouldLaunch = Player1;
            }
            else
            {
                MinDistance = Player2Distance;
                WouldLaunch = Player2;
            }
            
            if (MinDistance < ShouldLaunch_MaxDistance)
            {
                if (LastPlayerToLaunch != WouldLaunch)
                {
                    Launch(WouldLaunch);
                }
            }
            
            
            // Track if the last player to launch has left the launchable region
            // so we don't just launch on repeat while they're nearby
            if (LastPlayerToLaunch != null)
            {
                float DistanceToLastLauncher = (LastPlayerToLaunch.position - transform.position).sqrMagnitude;
                if (DistanceToLastLauncher >= ShouldLaunch_MaxDistance)
                {
                    LastPlayerToLaunch = null;
                }
            }
            
            yield return null;
        }
    }
    
    public void Launch(Transform WouldLaunch)
    {
        // TODO(Peter): have this actually launch the inventory item
        LastPlayerToLaunch = WouldLaunch;
        CatapultAnimator.SetTrigger(Launch_AnimatorTriggerName);
        
        Vector3 LaunchDirection = (LaunchForce_Direction.position - transform.position).normalized;
        
        Rigidbody2D RB = null;
        for (int c = 0; c < Trigger.OverlappingCollidersCount; c++)
        {
            RB = Trigger.OverlappingColliders[c];
            RB.AddForce(LaunchDirection * LaunchForce_Thrust);
        }
        
        Creak.Play();
    }
}