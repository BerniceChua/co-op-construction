using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildObject : MonoBehaviour {
    
    [System.NonSerialized]
        public bool isHeld, inRangeOfPlayer;
	
    public float MaxPickupDistance;
    
    [System.NonSerialized]
        public Transform[] PlayerXforms;
	
    public float zRot;
    
    public interface_block InterfaceBlock;
    
	// Use this for initialization
	public void Construct (Transform Player1, Transform Player2) 
    {
        PlayerXforms = new Transform[2] { Player1, Player2 };
        
		isHeld = false;
        InterfaceBlock.HighlightOff();
		zRot = 0f;
	}
	
	// Update is called once per frame
	void Update () {
		if (!isHeld)
        {
            closest_player_result ClosestPlayer = GetClosestPlayer();
            
            if (ClosestPlayer.Distance < MaxPickupDistance) 
            {
                ClosestPlayer.Player.GetComponent<character_movement>().SetSelectedBuildObject(this);
                InterfaceBlock.HighlightOn();
            }
            else
            {
                InterfaceBlock.HighlightOff();
            }
        } 
        else 
        {
            InterfaceBlock.HighlightOff();
        }
    }
    
    public class closest_player_result
    {
        public Transform Player;
        public float Distance;
    }
    
    public closest_player_result GetClosestPlayer()
    {
        closest_player_result Result = new closest_player_result();
        Result.Distance = 10000;
        Result.Player = null;
        
        for (int i = 0; i < PlayerXforms.Length; i++)
        {
            float Distance = (PlayerXforms[i].position - transform.position).sqrMagnitude;
            if (Distance < Result.Distance)
            {
                Result.Distance = Distance;
                Result.Player = PlayerXforms[i];
            }
        }
        
        return Result;
    }
    
    public void Rotate(int whichWay){
        zRot += Time.deltaTime * whichWay * 120;
        transform.rotation = Quaternion.Euler (0f, 0f, zRot);
    }
}
