using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class physics_object_destroy_trigger : MonoBehaviour {
    
    public void OnTriggerEnter2D (Collider2D Other)
    {
        BuildObject BO = Other.gameObject.GetComponent<BuildObject>();
        if (BO)
        {
            Destroy(BO.gameObject);
        }
    }
}
