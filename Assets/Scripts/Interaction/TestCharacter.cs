using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCharacter : MonoBehaviour {
    
	[SerializeField]
        private float moveSpeed;
    
	public List<GameObject> inRange;
    
	public PlayerMode playerMode;
    
	// Use this for initialization
	void Start () {
		inRange = new List<GameObject> ();
		playerMode = PlayerMode.Running;
	}
	
	// Update is called once per frame
	void Update () {
        
		if (playerMode == PlayerMode.Running) {
			//DisplayAllInRange ();
			float horizMove = Input.GetAxisRaw ("Horizontal") * moveSpeed;
			transform.position += new Vector3 (horizMove, 0f, 0f);
			if (Input.GetKeyDown (KeyCode.P) && inRange.Count > 0) {
				//pick up last object added to inRange list
				//should be the closest object i think?
				GameObject toPickUp = inRange[inRange.Count-1];
				inRange.RemoveAt (inRange.Count - 1);
				LiftObject (toPickUp);
				playerMode = PlayerMode.Holding;
			}
		}
		if (playerMode == PlayerMode.Holding) {
			if (Input.GetKey (KeyCode.G))
				Rotate (-1);
			else if (Input.GetKey (KeyCode.F))
				Rotate (1);
			if (Input.GetKeyDown (KeyCode.D)) {
				DropHeldObject ();
			}
		}
        
        
	}
    
	void LiftObject(GameObject which){
		which.GetComponent<Rigidbody2D> ().isKinematic = true;
		which.GetComponent<BuildObject> ().isHeld = true;
		which.transform.position = transform.position + new Vector3 (0f, 2f, 0f);
		which.GetComponent<BuildObject> ().zRot = which.transform.localEulerAngles.z;
		//GameObject.FindObjectOfType<ObjectMover> ().PickUp (which);
	}
    
	void DropHeldObject(){
		// Deprecated
        //GameObject obj = GameObject.FindObjectOfType<ObjectMover> ().GetSelected();
		//obj.GetComponent<Rigidbody2D> ().isKinematic = false;
        
		//obj.GetComponent<BuildObject> ().isHeld = false;
		playerMode = PlayerMode.Running;
	}
    
	void DisplayAllInRange(){
		foreach (GameObject obj in inRange) {
			// Deprecated
            //obj.GetComponent<BuildObject> ().ShowPickupPrompt (true);
            
		}
	}
    
	void Rotate(int whichWay){
        // Deprecated
		//GameObject.FindObjectOfType<ObjectMover> ().GetSelected().GetComponent<BuildObject>().Rotate (whichWay);
	}
}
