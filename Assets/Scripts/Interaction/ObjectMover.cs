using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

public class held_block
{
    public GameObject Instance_Block;
    public Rigidbody2D BlockRB;
    public BuildObject BuildObject;
    
    public character_movement Holder;
}

public class ObjectMover : MonoBehaviour {
    
	[SerializeField]
        private GameObject selectedObject;
    
	InputDevice player1Device;
    
	[SerializeField]
        private float moveSpeed;
    
    [System.NonSerialized]
        public held_block[] HeldBlocks;
    [System.NonSerialized]
        public int HeldBlocksCount;
    
    public Coroutine MoveObjectsUpdate;
    
    public void Construct ()
    {
        HeldBlocks = new held_block[2];
        HeldBlocksCount = 0;
    }
    
	public void PickUp(GameObject obj, character_movement Holder)
    {
        if(HeldBlocksCount >= HeldBlocks.Length)
        {
            Debug.LogError("Holding too many blocks already. How did this happen?");
            return;
        }
        
        held_block NewBlock = new held_block();
        NewBlock.Instance_Block = obj;
        NewBlock.BlockRB = obj.GetComponent<Rigidbody2D>();
        NewBlock.BuildObject = obj.GetComponent<BuildObject>();
        NewBlock.Holder = Holder;
        
        NewBlock.BlockRB.isKinematic = true;
        NewBlock.BuildObject.isHeld = true;
        NewBlock.BuildObject.zRot = NewBlock.Instance_Block.transform.localEulerAngles.z;
        NewBlock.Instance_Block.transform.position = Holder.transform.position + new Vector3(0, 2, 0);
		
        HeldBlocks[HeldBlocksCount++] = NewBlock;
        
        if (MoveObjectsUpdate == null)
        {
            MoveObjectsUpdate = StartCoroutine(CoMoveObjectsUpdate());
        }
	}
    
    public void PutDown (character_movement Holder)
    {
        int Index = -1;
        for (int i = 0; i < HeldBlocksCount; i++)
        {
            if (HeldBlocks[i].Holder == Holder)
            {
                Index = i;
                break;
            }
        }
        
        if (Index >= 0 && Index < HeldBlocksCount)
        {
            held_block RemoveBlock = HeldBlocks[Index];
            RemoveBlock.BlockRB.isKinematic = false;
            RemoveBlock.BuildObject.isHeld = false;
            
            // Shift down to fill the gap
            for (int i = Index; i < HeldBlocksCount - 1; i++)
            {
                HeldBlocks[i] = HeldBlocks[i+1];
            }
            HeldBlocks[HeldBlocksCount - 1] = null;
            HeldBlocksCount--;
        }
        
        if (HeldBlocksCount == 0 && MoveObjectsUpdate == null)
        {
            StopCoroutine(MoveObjectsUpdate);
        }
    }
    
    public IEnumerator CoMoveObjectsUpdate () 
    {
        while (true)
        {
            held_block Block;
            for (int i = 0; i < HeldBlocksCount; i++)
            {
                Block = HeldBlocks[i];
                
				if (Input.GetKey(Block.Holder.leftrightkeys[0]) ||
					(Block.Holder.inputDevice != null && Block.Holder.inputDevice.DPadLeft.IsPressed))
                {
                    Block.Instance_Block.transform.position += Vector3.right * -1 * moveSpeed * Time.deltaTime;
                }
				else if (Input.GetKey(Block.Holder.leftrightkeys[1]) ||
					(Block.Holder.inputDevice != null && Block.Holder.inputDevice.DPadRight.IsPressed))
                {
                    Block.Instance_Block.transform.position += Vector3.right * moveSpeed * Time.deltaTime;
                }else if (Input.GetKey(Block.Holder.KeyCode_NextInventoryItem) ||
					(Block.Holder.inputDevice != null && Block.Holder.inputDevice.DPadUp.IsPressed))
				{
					Block.Instance_Block.transform.position += Vector3.up * moveSpeed * Time.deltaTime;
				}
				else if (Input.GetKey(Block.Holder.KeyCode_SelectInventoryItem) ||
					(Block.Holder.inputDevice != null && Block.Holder.inputDevice.DPadDown.IsPressed))
				{
					Block.Instance_Block.transform.position += Vector3.up * -1 * moveSpeed * Time.deltaTime;
				}
                
				if (Input.GetKey(Block.Holder.KeyCode_RotateObject) ||
					(Block.Holder.inputDevice != null && Block.Holder.inputDevice.Action3.IsPressed))
                {
                    Block.BuildObject.Rotate(1);
                }
            }
            yield return null;
        }
    }
}
