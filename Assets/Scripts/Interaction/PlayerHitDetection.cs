using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHitDetection : MonoBehaviour {
    
	public float health;
    
	private character_movement cmv;
    
	public void Construct (character_movement CM) {
		cmv = CM;
		Debug.Log ("Contsructing PlayerHitDetection");
	}
    
	private void OnCollisionEnter2D(Collision2D other) {
		
		//TODO: Add object filtering
        health -= other.relativeVelocity.magnitude;
        
        Debug.LogFormat("Tap! " + other.relativeVelocity.magnitude + " remaining health " + health);
        
		if(health < 0) {
			Debug.Log("DEAD!");
			Destroy(gameObject);
		}
	}
    
}
