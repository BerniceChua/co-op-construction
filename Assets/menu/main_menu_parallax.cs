using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class main_menu_parallax : MonoBehaviour {
    
    // NOTE(Peter): these are in order with the back most layer being 0, and the 
    // top most bein n.
    // Layer 0 will not move at all and its position will be used as the root;
    public Transform[] ParallaxLayers;
    [System.NonSerialized]
        public Vector3[] ParallaxLayerStartPositions;
    
    // TODO(Peter): Actually use these values to scale parallax effect
    public float[] ParallaxLayerOffsets;
    
    public Transform ParallaxControlPoint;
    public float ParallaxScale;
    
    public Coroutine ParallaxUpdate;
    
    // TODO(Peter): get rid of this, call it from code
	void Awake ()
    {
        StartParallax();
    }
    
    public void StartParallax ()
    {
        if (ParallaxLayers.Length == 0 ||
            ParallaxLayerOffsets.Length != ParallaxLayers.Length ||
            ParallaxControlPoint == null)
        {
            Debug.LogError("Menu Parallax Controller is missing something. Layers: " + 
                           ParallaxLayers.Length + " Layer Offsets: " + ParallaxLayerOffsets.Length + 
                           " Controller: " + ParallaxControlPoint);
        }
        
        // Initialize Start Positions For Layers
        ParallaxLayerStartPositions = new Vector3[ParallaxLayers.Length];
        for (int l = 0; l < ParallaxLayers.Length; l++)
        {
            ParallaxLayerStartPositions[l] = ParallaxLayers[l].position;
        }
        
        if (ParallaxUpdate == null)
        { 
            ParallaxUpdate = StartCoroutine(CoParallaxUpdate());
        }
    }
    
    public void EndParallax ()
    {
        if (ParallaxUpdate != null)
        {
            StopCoroutine(ParallaxUpdate);
        }
    }
    
    IEnumerator CoParallaxUpdate ()
    {
        while (true)
        {
            Vector3 root_position = ParallaxLayers[0].position;
            Vector3 from_back_to_focus = ParallaxControlPoint.position - root_position;
            
            Transform layer;
            Vector3 start_position;
            for (int i = 0; i < ParallaxLayers.Length; i++)
            {
                start_position = ParallaxLayerStartPositions[i];
                layer = ParallaxLayers[i];
                layer.position = Vector3.Lerp(start_position, 
                                              (from_back_to_focus * ParallaxScale),  
                                              (float)i/ParallaxLayers.Length); 
            }
            
            yield return null;
        }
    }
    
}
