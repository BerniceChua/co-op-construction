using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class temp_mouse_parallax_controller : MonoBehaviour 
{
    
    public Animator MenuAnimation;
    
    public Coroutine MouseControlUpdate;
    
    void Awake ()
    {
        StartMouseControl();
    }
    
    public void StartMouseControl ()
    {
        if (MouseControlUpdate == null)
        {
            MouseControlUpdate = StartCoroutine(CoMouseControlUpdate());
        }
    }
    
    public void StopMouseControl ()
    {
        if (MouseControlUpdate != null)
        {
            StopCoroutine(MouseControlUpdate);
        }
    }
    
    IEnumerator CoMouseControlUpdate ()
    {
        while (true)
        {
            float MouseXPercent = (float)Input.mousePosition.x / (float)Screen.width;
            MouseXPercent = 1.3f - (MouseXPercent * 1.6f);
            MouseXPercent = Mathf.Clamp(MouseXPercent, 0.0f, 1.0f);
            
            MenuAnimation.SetFloat("ParallaxBlend", MouseXPercent);
            
            if (Input.GetMouseButtonDown(0))
            {
                SceneManager.LoadSceneAsync("game_level", LoadSceneMode.Single);
            }
            
            yield return null;
        }
    }
    
}
