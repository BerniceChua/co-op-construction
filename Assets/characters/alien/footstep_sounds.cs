using UnityEngine;

public class footstep_sounds : MonoBehaviour
{
    private int LastPlayedSound = 0; 
    public AudioClip[] StepSounds;
    public AudioSource StepPlayer;
    
    public void PlayNextStep ()
    {
        int NextSound = LastPlayedSound + 1;
        if (NextSound >= StepSounds.Length)
        {
            NextSound= 0;
        }
        StepPlayer.PlayOneShot(StepSounds[NextSound]);
        
        LastPlayedSound = NextSound;
    }
}