# Flotsam And Jetsam (formerly known as "Co-op Construction")
A couch-co-op game where two players help each other build shelter to protect against an impending cataclysm.

![Splash image of the game "Flotsam And Jetsam"](https://ggj.s3.amazonaws.com/styles/game_sidebar__wide/featured_image/2019/01/195778/splash_0.png?itok=QnNco8rp&timestamp=1548637438)

![Screenshot of the gameplay of "Flotsam And Jetsam"](https://ggj.s3.amazonaws.com/styles/feature_image__wide/games/screenshots/in_game_sc_0.png?itok=65MzMPCN&timestamp=1548637438)

## About
This game was our project for the [Global Game Jam 2019 (the weekend of January 25 to January 27)](https://globalgamejam.org/2019/jam-sites/noisebridge).  The [theme](https://globalgamejam.org/news/theme-global-game-jam-2019-%E2%80%A6) of Global Game Jam 2019 was: ["What home means to you"](https://www.youtube.com/watch?v=pUohwjq9RkA).

![Global Game Jam 2019 Theme](https://globalgamejam.org/sites/default/files/styles/responsive_large__wide/public/field_news_story_image_video/ggj19_news_themereveal_708x432px_v2.jpg?itok=R9Bs83zH&timestamp=1548515785)

### Project Link
https://globalgamejam.org/2019/games/flotsam-and-jetsam

### Design Doc
https://docs.google.com/document/d/1fzTpPGZ8jxwCsvQIvyt95XHhiySW7M6OeT3tZSj4Aos/edit?ts=5c4bd387

### Technologies Used
- Unity3D
- C#
- git
- GitLab

### Contributor List (in alphabetical order) - Who Made This?
H&#233;l&#232;ne Choyer (composer, musician, sound designer)
- https://www.helenechoyer.com/
- https://www.linkedin.com/in/h%C3%A9l%C3%A8ne-choyer-3a9533177/

Bernice Anne W. Chua (programmer, UX/UI, local GGJ co-organizer*)
- https://www.aaronswartzday.org/bernice-chua/
- http://bernicechua.github.io/
- https://gitlab.com/BerniceChua
- https://linkedin.com/in/bernicechua415/
- https://twitter.com/ChuaBernice
- https://www.noisebridge.net/wiki/User:Berni

Elaine Gilstrom (programmer)
- https://github.com/ElaineGilstrom

Peter Slattery (artist, illustrator, programmer)
- http://www.peter-slattery.com/

Mark H. Willson (programmer, project manager, local GGJ co-organizer*)
- https://twitter.com/MarkHWillson

<sup>*Mark H. Willson & Bernice Anne W. Chua were also organizers of the local Global Game Jam 2019 site at [Noisebridge](https://noisebridge.net), aside from participants in the game jam itself.</sup>


## Roadmap
- Improve the UI and the controls.
- We would like to make builds for other systems: Linux/Unix, & Apple Mac, & WebGL.
- Upload to itch.io.


## Feedback
We welcomes any and all the feedbacks! ^_^  You can use the above links to contact the people involved in this project.

OR

Please send feedback by [creating a new Issue](https://gitlab.com/BerniceChua/co-op-construction/issues/new), or by [clicking on the Issues link on the sidebar](https://gitlab.com/BerniceChua/co-op-construction/issues).  Feedback can also be sent to Bernice through the links under their name in the contributor list.
